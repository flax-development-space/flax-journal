---
title: Innovation and technology
baseline: "eLife develops and invests in technology that enhances the sharing and use of research results online.."
metatitle: Innovation and technology
metalinktext: Our open-source technology
metaintro: eLife is developing open- source platforms that support the review, organisation and dissemination of the research literature.
metaimg: technology.png
relatedLinks: 
  - "[eLife Latest: Announcing a new technology direction](https://elifesciences.org/inside-elife/daf1b699)"
  - "[Sciety.org](https://sciety.org/)"
---

eLife invests in the development of platforms that support the display, review, and organisation, dissemination and curation of content.

From considering how to publish preprint content in an enhanced form to developing a system that supports an end-to-end workflow for reviewing preprints, our technology efforts are fully aligned with eLife’s goal to transform research communication by transitioning to a publish, review, curate model of publishing.

An important step towards this goal is the development of [Sciety](https://sciety.org/), an online application for public preprint evaluation. Built by the team at eLife, Sciety brings together the latest biomedical and life science preprints that are transparently evaluated and curated by communities of experts in one convenient place.

All software developed at eLife is open source under the most permissible of licences and can be found in our GitHub organisations for [eLife GitHib](https://github.com/elifesciences) and [Sciety GitHub](https://github.com/sciety).

