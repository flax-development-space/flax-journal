---
title: "Our mission" 
baseline: "Reviewed Preprints combine the advantages of preprints with the scrutiny offered by peer review."
metatitle: "Our mission"
metalinktext: "Our vision for the future"
metaintro: eLife is committed to bringing about real change in the way the results of research are reviewed and communicated..
metaimg: "our-mission.png"
---

eLife is committed to creating a future where a diverse, global community of scientists and researchers shares open and trusted results for the benefit of all.

To achieve this, we are developing a new model of publishing that puts preprints first and emphasises public reviews and assessments of research. This system makes sharing of research faster and fairer, while still providing robust peer review and quality assessment.

We are working towards our mission in three ways – with the eLife journal, through the development of open-source technology, and via our community-engagement activities.

  

![eLife's mission is Helping scientists accelerate discovery by operating a platform for research communication that encourages and recognises the most responsible behaviours in science.](https://resize-v3.pubpub.org/eyJidWNrZXQiOiJhc3NldHMucHVicHViLm9yZyIsImtleSI6Ijh0aWN6dHZvLzAxNjc1MDkwNTkzODMxLmpwZyIsImVkaXRzIjp7InJlc2l6ZSI6eyJ3aWR0aCI6ODAwLCJmaXQiOiJpbnNpZGUiLCJ3aXRob3V0RW5sYXJnZW1lbnQiOnRydWV9fX0=)

eLife’s mission displayed in the Cambridge (UK) office.
