---
title: Research culture
baseline: "eLife recognises that reforming research communication depends on improving research culture."
metatitle: Improving research culture
metalinktext: Initiatives for the community
metaintro: The eLife Communities team works to promote a research culture that values openness, integrity and equity, diversity and inclusion.
metaimg: research-culture.png
relatedLinks:
  - "[Community page](https://elifesciences.org/community)"
  - "[ecrLife](https://ecrlife.org/)"
  - "Follow [eLife Community on Twitter](https://twitter.com/elifecommunity)"
  - "Sign up for our [early-career researchers newsletter](https://elifesciences.org/content-alerts/early-career)"
---

eLife has an ambitious agenda to reform how research is communicated and assessed, and works to promote a research culture that centres on openness, integrity, and equity, diversity and inclusion. Supported by our [Communities team](https://elifesciences.org/for-the-press/99a91a3b), we engage closely with researchers across biology and medicine to drive this change. Updates on many of these activities can be found on our [Community page](https://elifesciences.org/community).

In parallel, eLife publishes [articles on research culture](https://elifesciences.org/collections/edf1261b) and [equity, diversity and inclusion](https://elifesciences.org/collections/3a6a7db3), and provides a platform for the research community to discuss relevant issues through [personal stories](https://elifesciences.org/collections/1926c529), [interviews](https://elifesciences.org/interviews), [podcasts](https://elifesciences.org/podcast) and [webinars.](https://elifesciences.org/collections/842f35d5)

eLife is deeply committed to helping make research and publishing more equitable and inclusive, and we regularly [report on our actions in these areas](https://elifesciences.org/inside-elife/721407a3).

With the guidance of our [Early-Career Advisory Group](https://elifesciences.org/about/people/early-career), eLife has created peer networks through the [eLife Community Ambassadors program](https://elifesciences.org/inside-elife/f744fae0), [supported researchers](https://elifesciences.org/inside-elife/c14838ac) from underrepresented backgrounds and countries with limited funding, and [increased the involvement of early-career researchers in peer review](https://reviewer.elifesciences.org/reviewer-guide/review-process#involvement-early-career-researchers-peer-review).
