---
title: "Welcome"
layout: pages/homepage.njk
permalink: "/flax/"
classes: "flax-home"
menufooter: What is Flax
order: 1
---

Welcome to Flax simple migration demo.

When running: `npm run kotahimigration` in the root of the project, you will be asked for the ISSN for the journal you
want to build a site for.

It will then load the data from crossref and generate the website.

And it will open your browser right where it need to be.
