<!-- manag to pull the file, now let’s find them all :D -->

use 11ty navigation plugin



so.

`node kotahi_simpleMigration.js` will load 100 articles and put them in a global data file in the data folder in eleventy 

then `npm run serve` will generate the website  and make it available through `localhost:8080`

the content is in `src/content`

<!-- the layout are separated in block: `elife`, `common`, etc. Those files are gonna be part of the main flax repo: the idea is : when someone create flax, it define a set of folder, and we leverage eleventy.before actions to make those layouts available on build/serve time. -->

<!-- which means that, if you want to override them, you simple create a layout with the same name in your rpo. -->

<!-- there a bit of work to make that happens, we’ll jump on that when we’re done with the neuro science experiment. -->
