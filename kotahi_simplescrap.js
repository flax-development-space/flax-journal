// const { exec } = require('child_process');
const fs = require("fs");
const { got } = require("got-cjs");
const jsdom = require("jsdom");
const yaml = require("js-yaml");

const { JSDOM } = jsdom;

const urls = [
  "https://doi.org/10.11157/anzswj-vol34iss1id846",
  "https://doi.org/10.11157/anzswj-vol34iss1id846",
];

scrap(urls);

async function scrap(urls) {
  urls.forEach(async (url, index) => {
    got(url, {
      headers: {
        "User-Agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3",
      },
    })
      .then(async (response) => {
        try {
          // await getLogo(response.body, index);
          await createSinglePage(response.body, index);
          console.log(`${url} process completed`);
        } catch (error) {
          console.log(`${url} an error occurred`, error);
        }
      })
      .catch((err) => {
        console.log(`${url} error in url access : ${err}`);
      });
  });
}

async function getLogo(content, index) {
  return "logo";
  // content.querySelector("")
}

async function createSinglePage(content, index) {
  //clean the html output
  let htmlWithoutSpace = content.replace(/>(\s+|\nr)(?![^<>]*<\/)/g, "> ");
  htmlWithoutSpace = htmlWithoutSpace.replace(/(\r\n|\n|\r)/g, "");
  htmlWithoutSpace = htmlWithoutSpace.replace(/\t/g, "");

  let doc = new JSDOM(htmlWithoutSpace);
  let document = doc.window.document;

  //get the content
  let articleContent = document.documentElement.querySelector(".page_article");

  // get the doi =
  const doi = document.documentElement
    .querySelector(".item.doi >.value a")
    .innerHTML.replace("https://doi.org/", "");

  // get the title
  const title = yaml.dump(
    document.querySelector(".page_title").innerHTML.trim()
  );

  //get the subtitle
  const subtitle = getSubtitle(document.querySelector(".subtitle"));
  //remove the subtitle
  document.querySelector(".subtitle")?.remove();

  //citation
  const citationOutput = yaml.dump(
    document.querySelector("#citationOutput").innerHTML
  );

  //pubdate
  const pubdate = yaml.dump(document.querySelector(".published").innerHTML);

  //remove block we get in the meta data
  const toRemoveBlocks = [
    "nav",
    ".galleys",
    ".published",
    ".cmp_breadcrumbs",
    ".page_title",
    ".doi",
    ".issue",
    ".citation",
  ];

  removeBlocks(toRemoveBlocks, document.documentElement);

  //write content and folders

  // Check if the folder exists
  if (!fs.existsSync(`./src/content/articles/${doi}`)) {
    // Create the folder
    fs.mkdirSync(`./src/content/articles/${doi}`, { recursive: true });
    console.log(`Folder for articles scrap created successfully.`);
  } else {
    // console.log(
    //   `Folder for issn: ${issn.replace("-", "")} already exists.`
    // );
  }

  fs.writeFileSync(
    `./src/content/articles/${doi}/scrap.html`,
    `---\n
layout: "pages/single-page.njk"
permalink: "/articles/${doi}/scrap.html" 
title: ${title.trim()}
${subtitle ? `subtitle: ${subtitle.trim()}` : ""}
pubdate: ${pubdate.trim()}
citationOutput: ${citationOutput.trim()} 
${getAuthorsAndAffiliations(document.querySelector(".authors"))}
${getReferences(document.documentElement.querySelector(".references .value"))}
${keywordsAsTags(document.documentElement.querySelector(".keywords > .value"))}
${getLicence(document.documentElement.querySelector(".copyright"))}
---\n\n
${articleContent.innerHTML.trim()}`,
    "utf8",
    (error) => {
      if (error) {
        console.log(error);
      } else {
        console.log("file writtten with success");
      }
    }
  );
}

function getAuthorsAndAffiliations(element) {
  let authorsList = "";
  element.querySelectorAll("li").forEach((li) => {
    authorsList += `  - name: ${li.querySelector(".name").innerHTML}
    affiliation: ${li.querySelector(".affiliation").innerHTML}\n`;
  });
  element.remove();
  return `authors:\n${authorsList}`;
}

// get references
function getReferences(element) {
  let referencesList = "";
  element.querySelectorAll("p").forEach((item) => {
    if (item) {
      referencesList += `  - '${item.innerHTML.trim()}'\n`;
    }
  });
  element.closest(".references").remove();
  return `references:\n${referencesList}`;
}

function getSubtitle(element) {
  if (!element) return "";
  let subtitleText = element.innerHTML.trim();
  element.remove;
  return yaml.dump(subtitleText);
}

function keywordsAsTags(element) {
  const tags = element.innerHTML.split(",");
  let tagsBlock = `tags:\n`;
  tags.forEach((tag) => {
    tagsBlock += `  - ${tag}\n`;
  });
  element.closest(".keywords").remove();
  return tagsBlock;
}

function removeBlocks(array, fromElement) {
  array.forEach((el) => {
    let element = fromElement.querySelector(el);
    if (!element) return;
    fromElement.querySelector(el).remove();
  });
}

function getLicence(element) {
  let content = element.innerHTML;
  element.remove();
  return `licence: ${yaml.dump(content.innerHTML)}`;
}
