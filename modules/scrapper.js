const fs = require("fs");
const { got } = require("got-cjs");
const jsdom = require("jsdom");
const yaml = require("js-yaml");
const { JSDOM } = jsdom;

async function crossrefScrap(url) {
  try {
    const document = await scrapHTML(url);
    const scrappedMeta = {
      logourl: await getLogo(document, ".pkp_site_name"),
      aboutContent: await getAbout(document, "#navigationPrimary", ".page"),
      teamContent: await getTeam(document, "#navigationPrimary", ".page"),
    };

    console.log(`\n${url} process completed`);

    return scrappedMeta;
  } catch (err) {
    console.log(`${url} error in url access: ${err}`);
  }
}

async function getLogo(content, imageClosest) {
  console.log("\nTrying to find the logo from the menu page");

  const menuElement = content.querySelector(imageClosest);

  if (!menuElement) {
    return 'placeholder'; // Return empty string if menu element is not found
  }

  let imageUrl = content.querySelector(imageClosest).querySelector("img");
  if (imageUrl) {
    return content.querySelector(imageClosest).querySelector("img").src;
  }
  console.log("\nLogo found");
  //  img: url = location for the logo
}

async function getTeam(content, menu, closestElement) {
  console.log("\nTrying to find the team page");
  const menuElement = content.querySelector(menu);

  if (!menuElement) {
    return 'placeholder'; // Return empty string if menu element is not found
  }

  let linksToAbout = Array.from(
    content.querySelector(menu).querySelectorAll("a")
  ).filter((a) => {
    if (a.href.endsWith("team") | a.href.endsWith("Team")) {
      return true;
    }
  });

  const aboutTeam = await Promise.all(
    linksToAbout.map(async (a) => {
      let content = await scrapHTML(a.href);
      return await pullHTML(content, closestElement);
    })
  );
  console.log("\nTeam page created");
  return aboutTeam[0];
}

async function getAbout(content, menu, closestElement) {
  console.log("Trying to find the about page");
  const menuElement = content.querySelector(menu);

  if (!menuElement) {
    return 'placeholder'; // Return empty string if menu element is not found
  }

  let linksToAbout = Array.from(
    content.querySelector(menu).querySelectorAll("a")
  ).filter((a) => {
    if (a.href.endsWith("about")) {
      return true;
    }
  });

  const aboutContents = await Promise.all(
    linksToAbout.map(async (a) => {
      let content = await scrapHTML(a.href);
      return await pullHTML(content, closestElement);
    })
  );
  return aboutContents[0];

  console.log("\nAbout page created");
}

async function pullHTML(document, element) {
  if (!element) console.log("no element found for " + element);
  // console.log(element)
  return document.querySelector(element).innerHTML;
}

async function scrapHTML(url) {
  const response = await got(url, {
    headers: {
      "User-Agent":
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3",
    },
  });

  let htmlWithoutSpace = response.body.replace(
    />(\s+|\nr)(?![^<>]*<\/)/g,
    "> "
  );
  htmlWithoutSpace = htmlWithoutSpace.replace(/(\r\n|\n|\r)/g, "");
  htmlWithoutSpace = htmlWithoutSpace.replace(/\t/g, "");

  const doc = new JSDOM(htmlWithoutSpace);
  const document = doc.window.document;

  // Remove nav element
  const navElement = document.documentElement.querySelector(".page nav");
  if (navElement) {
    navElement.remove();
  }
  return document.documentElement;
}

module.exports = { crossrefScrap };
