# the roadmap

## ojs world

1 use the issn to get the content from crossref.
2 generate index and article page with the available content


2bis find the hardcoded bit and remove it

3. if the journal has the article available, scrap the content into a single html fie and make it available to the tool

4. search?
5. pages around 
6 that’s it!




## elife world

1. get the collection from elifescience.api
2. get a collection out of it
3. start a flax instance
4. print on the fly using paged.js
5. api call from flax to get some article (a hundred?)
6. add features one by one (search, etc.)

## get the data from api

## the system

the page to adds:

- the home page with 3 latest article, a block about the team, a blog contact, etc.
- about page that contains a bit of data manipulated through a netlify cms.
- index page containing all the articles, with pagination and filtering (author, type, title, abstract, etc.)

## the table of content eleventy plugin:

problem: it’s slow:

<!-- but on transform open the html. check for all titles, and create a link to it. -->

## notes

- toc
- figure and data tab
- numbers of views: would need a matomo or something to keep the numbering

-
