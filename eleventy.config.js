const { EleventyHtmlBasePlugin } = require("@11ty/eleventy");
const eleventyNavigationPlugin = require("@11ty/eleventy-navigation");

const flaxHelpers = require("./11ty-plugins/flax-helpers.js");

// for automatic start of the server
const PORT = 8100; // use a port you are reasonably sure is not in use elsewhere

module.exports = function (eleventyConfig) {
  console.log(process.env.ELEVENTY_RUN_MODE);

  // quiet
  //journal" + issn
  eleventyConfig.setQuietMode(true);

  //server
  // only copy file on build. otherwise fake the copy
  eleventyConfig.setServerPassthroughCopyBehavior("passthrough");

  eleventyConfig.setServerOptions({
    // to auto reload when css change
    watch: ["public/**/*.css", "static/**/*.css"],
  });

  if (process.env.ELEVENTY_RUN_MODE != "serve") {
    eleventyConfig.on("eleventy.after", async () => {
      // Run me after the build ends
      require("out-url").open(`http://localhost:${PORT}`);
    });

    eleventyConfig.setServerOptions({
      port: PORT,
    });
  }

  // add base html to every url
  eleventyConfig.addPlugin(EleventyHtmlBasePlugin);

  // flaxhelpers
  eleventyConfig.addPlugin(flaxHelpers);

  // passthrough file copy //
  eleventyConfig.addPassthroughCopy(
    { "static/": "assets/" },
    {
      expand: true,
    }
  );
  // eleventyConfig.addPassthroughCopy({ "static/fonts": "css/fonts" });

  // navigation plugin
  eleventyConfig.addPlugin(eleventyNavigationPlugin);

  //content page collections
  eleventyConfig.addCollection("aboutPages", function (collectionApi) {
    return collectionApi.getFilteredByGlob("./src/content/pages/about/*.md");
  });

  // create a collection for all the page that have menu in there frontmatter
  eleventyConfig.addCollection("menu", function (collectionApi) {
    let collection = collectionApi
      .getFilteredByGlob("./src/content/**/*.md", "./src/content/articles/**/*.html")
      .filter(function (item) {
        if (item.data.menu) {
          return item;
        }
      })
      .sort(function (a, b) {
        return b.order - a.order;
      });
    return collection;
  });


  eleventyConfig.addCollection("menufooter", function (collectionApi) {
    let collection = collectionApi
      .getFilteredByGlob("./src/content/**/*.md", "./src/content/**/*.html")
      .filter(function (item) {
        if (item.data.menufooter) {
          return item;
        }
      })
      .sort(function (a, b) {
        return b.order - a.order;
      });
    return collection;
  });

   
  return {
    // templates engines : njk
    markdownTemplateEngine: "njk",

    // set the directories
    dir: {
      input: "src",
      output: "public",
      includes: "layouts",
      data: "data",
    },
  };
};

// to do
//
//
// 1. set collections
// in the plugin section
// 2. set plugins
// 3.
//
// f
