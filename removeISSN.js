const readline = require("readline");
const fs = require("fs");
const path = require("path");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const journalsDirectory = "./src/data/journals";

fs.readdir(journalsDirectory, (err, files) => {
  if (err) {
    console.error("Error reading journals directory:", err);
    rl.close();
    return;
  }

  issns.foreach((item, index) => {
    if (item.includes("meta")) {
      issns.splice(index, 1);
    }
  });
  const issns = files.map((file) => {
    return file.replace(".json", "");
  });

  console.log(issns);
  issns.foreach((item, index) => {
    if (item.includes("meta")) {
      issns.splice(index, 1);
    }
  });

  const issnList = issns.join(", ");

  console.log("ISSN List:");
  issns.forEach((issn, index) => {
    console.log(`${index + 1}. ${issn}`);
  });

  rl.question(
    `Enter the number for the ISSN or type "all" to backup/remove all files (${issnList}): `,
    (input) => {
      if (input === "all") {
        backupAndRemoveAllFiles(issns);
      } else {
        const selectedIndex = parseInt(input) - 1;
        if (
          !isNaN(selectedIndex) &&
          selectedIndex >= 0 &&
          selectedIndex < issns.length
        ) {
          const issn = issns[selectedIndex];
          backupAndRemoveFiles(issn);
        } else {
          console.log(`Invalid input: ${input}`);
        }
      }

      rl.close();
    }
  );
});

function backupAndRemoveFiles(issn) {
  const journalIndexPath = `./src/content/journals/${issn}-index.md`;
  const journalArticlesPath = `./src/content/journals/${issn}-articles.md`;
  const journalDataPath = `./src/data/journals/${issn}.json`;
  const journalDataMetaPath = `./src/data/journals/${issn}meta.json`;
  const backupFolderPath = path.join(__dirname, "backup");

  backupFile(journalIndexPath, backupFolderPath);
  backupFile(journalArticlesPath, backupFolderPath);
  backupFile(journalDataPath, backupFolderPath);
  backupFile(journalDataMetaPath, backupFolderPath);

  removeFile(journalIndexPath);
  removeFile(journalArticlesPath);
  removeFile(journalDataPath);
  removeFile(journalDataMetaPath);
}

function backupAndRemoveAllFiles(issns) {
  issns.forEach((issn) => {
    backupAndRemoveFiles(issn);
  });
}

function backupFile(filePath, backupFolderPath) {
  const fileDir = path.dirname(filePath);
  const backupDir = path.join(backupFolderPath, fileDir);

  fs.mkdirSync(backupDir, { recursive: true });
  fs.copyFileSync(filePath, path.join(backupFolderPath, filePath));
  console.log(`Backed up ${filePath} to ${backupDir}`);
}

function removeFile(filePath) {
  fs.unlinkSync(filePath);
  console.log(`Removed ${filePath}`);
}
