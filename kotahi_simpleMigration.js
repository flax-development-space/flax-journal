const fs = require("fs");
const axios = require("axios");
const path = require("path");
const readline = require("readline");
const { exec } = require("child_process");
const minimist = require("minimist");
const { SingleBar } = require("cli-progress");
const { crossrefScrap } = require("./modules/scrapper");

async function crossrefApiCall(issn) {
  // the content for the each article for Flax
  let articleContent = `---
title: "The article" 
issn: "${issn.replace("-", "")}"
pagination:
  data: journals[${issn.replace("-", "")}]
  size: 1
  alias: article
permalink: "/articles/{{issn}}/{{article.DOI}}/index.html"
layout: "crossref/crossref-journal-single.njk"
--- `;

  let volumeIndex = `---
title: "Back issues"
issn: "${issn.replace("-", "")}"
permalink: "/articles/${issn.replace("-", "")}/volumes/index.html"
layout: "crossref/crossref-volume-index.njk"
menu: "Back issues"
---`;

  // the content for the archive pages in flax
  let archiveContent = `---
title: "Archives"
issn: "${issn.replace("-", "")}"
pagination:
  data: journals[${issn.replace("-", "")}]
  size: 10
  alias: article
permalink: "/articles/{{issn}}/archives/{{pagination.pageNumber}}/index.html"
layout: "crossref/crossref-journal-archive.njk"
menu: "Archives"
--- `;

  // the content for the current page [the latest issue?] chosen by the layout file.
  // we could change the layout from a cli question: order?
  let currentContent = `---
title:  "Current"
issn: "${issn.replace("-", "")}"
layout: crossref/crossref-journal-index.njk
permalink: "/articles/${issn.replace("-", "")}/index.html"
menu: "Current"
---
`;

  // the content for the health page in flax
  let indexHealth = `---
title: health check
issn: "${issn.replace("-", "")}"
layout: crossref/crossref-meta-health-check.njk
permalink: "/articles/${issn.replace("-", "")}/health.html"
menufooter: "health check"
---`;

  // the content for the about page in flax
  let aboutPage = `---
title: "About"
issn: "${issn.replace("-", "")}"
layout: crossref/crossref-about.njk
permalink: "/articles/${issn.replace("-", "")}/about.html"
menu: "About"
---`;

  // the content for the about page in flax
  let homePage = `---
title: "homepage"
issn: "${issn.replace("-", "")}"
layout: crossref/crossref-home.njk
permalink: "/"
---`;

  // the content for the about page in flax
  let teamPage = `---
title: "Team"
issn: "${issn.replace("-", "")}"
layout: crossref/crossref-team.njk
permalink: "/articles/${issn.replace("-", "")}/team.html"
menu: "Team"
---`;

  //instead of going to get all the works with a doi parameters we can use the ISSN for the journal

  const journalurl = `https://api.crossref.org/journals/${issn}/`;
  const url = `https://api.crossref.org/journals/${issn}/works?select=DOI,page,title,link,issue,volume,abstract,type,author&rows=1000`;
  const headers = {
    "User-Agent": "Coko/1.0 (julien@coko.foundation)",
  };

  // get info about the journal

  let metadata = await axios
    .get(journalurl, {
      headers: headers,
    })
    .then((res) => {
      console.log(
        `create a Journal metadata file for ISSN:${issn}.\n\n
your now downloading the metadata for  \n
${res.data.message.title} \n\n
published by \n${res.data.message.publisher}\n\n
please wait while we’re getting the data`
      );

      const journalJsonData = JSON.stringify(res.data.message);

      let randomtime = Math.random() * (1200 - 700) + 700;
      // console.log(randomtime);
      showProgress(randomtime);

      fs.writeFileSync(
        `src/data/journals/${issn.replace("-", "")}meta.json`,
        journalJsonData,
        "utf8"
      );

      console.log("\n\nThe journal metadata file has been successfully saved.");

      return res.data.message;
    });

  // get info about its content
  await axios
    .get(url, {
      headers: headers,
    })
    .then(async (res) => {
      console.log(
        `Journal ${issn} downloaded with a total of ${res.data.message["total-results"]} entries. 
\n\nGenerating meta data files and folders`
      );
      if (res.data.message["total-results"] > 1000) {
        console.log(
          "We’re using only 1000 entries to keep the api safe. Thanks for your understanding."
        );
      }

      console.log("Sorting journal per volumes and issues");
      // console.log(res.data.message.items);
      const articlejsonData = reorderPerIssue(res.data.message.items);
      const jsonData = JSON.stringify(articlejsonData);

      // scrap the meta page
      // if(getMetaPage) {
      //
      //
      // create a json file reorderedPerVolume

      const volumes = JSON.stringify(
        reconstructVolumes(res.data.message.items)
      );

      fs.writeFileSync(
        `src/data/journals/${issn.replace("-", "")}volumes.json`,
        volumes,
        "utf8"
      );

      const scrappedMeta = await scrapMetaPage([res.data.message.items[0].DOI]);

      // console.log(scrappedMeta);

      fs.writeFileSync(
        `src/data/journals/${issn.replace("-", "")}scrap.json`,
        scrappedMeta,
        "utf8"
      );
      console.log("Journal meta page file created");
      // }

      fs.writeFileSync(
        `src/data/journals/${issn.replace("-", "")}.json`,
        jsonData,
        "utf8"
      );
      console.log("Articles metadata file saved successfully.");

      // if the content/journals/ doesnt exist create it

      const folderPath = path.join(
        __dirname,
        "content",
        "journals",
        issn.replace("-", "")
      );

      // Check if the folder exists
      if (!fs.existsSync(folderPath)) {
        // Create the folder
        fs.mkdirSync(folderPath, { recursive: true });
        console.log(
          `Folder for issn: ${issn.replace("-", "")} created successfully.`
        );
      } else {
        // console.log(
        //   `Folder for issn: ${issn.replace("-", "")} already exists.`
        // );
      }

      // create the current articles page
      fs.writeFileSync(
        `src/content/journals/${issn.replace("-", "")}current.md`,
        currentContent,
        "utf8"
      );
      console.log("Current issue page created file successfully.");

      // create the current home page
      fs.writeFileSync(
        `src/content/journals/${issn.replace("-", "")}home.md`,
        homePage,
        "utf8"
      );

      console.log("home page created file successfully.");

      // create the archives article page
      //
      fs.writeFileSync(
        `src/content/journals/${issn.replace("-", "")}archives.md`,
        archiveContent,
        "utf8"
      );
      console.log("Archive page created file successfully.");

      fs.writeFileSync(
        `src/content/journals/${issn.replace("-", "")}articles.md`,
        articleContent,
        "utf8"
      );

      console.log("\n\nArticle page created successfully.");

      fs.writeFileSync(
        `src/content/journals/${issn.replace("-", "")}health.md`,
        indexHealth,
        "utf8"
      );

      console.log("\nHealth check page created successfully.");

      // create the archives article page
      fs.writeFileSync(
        `src/content/journals/${issn.replace("-", "")}about.md`,
        aboutPage,
        "utf8"
      );
      console.log("About page created successfully.");

      // create the archives per volume page
      fs.writeFileSync(
        `src/content/journals/${issn.replace("-", "")}backissues.md`,
        volumeIndex,
        "utf8"
      );
      console.log("About page created successfully.");

      // create the team page
      fs.writeFileSync(
        `src/content/journals/${issn.replace("-", "")}team.md`,
        teamPage,
        "utf8"
      );
      console.log("Team page created successfully.");
      return jsonData;
    })

    //create the eleventy files to make the content available
    //write index
    //write article page
    //fs

    .catch((err) => {
      console.log(err);
    });
}

async function removeAllEntries() {
  // remove all journals

  const journalsDirectory = "./src/data/journals";

  fs.readdir(journalsDirectory, (err, files) => {
    if (err) {
      console.error("Error reading journals directory:", err);
      rl.close();
      return;
    }

    const issns = files
      .filter((file) => {
        if (!file.includes("meta") || !file.includes("scrap")) {
          // console.log(file);
          return file;
        }
        return file;
      })
      .map((file) => file.replace(".json", ""));

    // console.log(issns);
    issns.forEach((item) => {
      if (item.includes("meta") || item.includes("scrap")) {
        return;
      }
      removeFiles(item);
    });
  });
}

async function removeAllFilesFromFolder(folderPath) {
  fs.readdir(folderPath, (err, files) => {
    if (err) {
      // console.error(`Error reading directory: ${err}`);
      return;
    }

    files.forEach((file) => {
      const filePath = path.join(folderPath, file);

      fs.unlink(filePath, (err) => {
        if (err) {
          // console.error(`Error deleting file: ${filePath}`);
        } else {
          // console.log(`Deleted file: ${filePath}`);
        }
      });
    });
  });
}

function removeFiles(issn) {
  issn = issn.replace("-", "");
  const journalDataPath = `./src/data/journals/${issn}.json`;
  const journalDataMetaPath = `./src/data/journals/${issn}meta.json`;
  const journalDataScrapPath = `./src/data/journals/${issn}scrap.json`;

  const journalIndexPath = `./src/content/journals/${issn}current.md`;
  const journalArchivePath = `./src/content/journals/${issn}archives.md`;
  const journalArticlesPath = `./src/content/journals/${issn}articles.md`;
  const journalDataHealth = `./src/content/journals/${issn}health.md`;
  const journalTeam = `./src/content/journals/${issn}team.md`;
  const journalAbout = `./src/content/journals/${issn}about.md`;
  const journalHome = `./src/content/journals/${issn}home.md`;

  removeFile(journalDataPath);
  removeFile(journalDataMetaPath);
  removeFile(journalDataScrapPath);

  removeFile(journalHome);
  removeFile(journalIndexPath);
  removeFile(journalArticlesPath);
  removeFile(journalDataHealth);
  removeFile(journalArchivePath);
  removeFile(journalAbout);
  removeFile(journalTeam);
}

function removeFile(filePath) {
  fs.statSync(filePath, function(err) {
    if (err) {
      return console.error(err);
    }

    fs.unlinkSync(filePath, function(err) {
      if (err) return console.log(err);
    });
  });
}

async function showProgress(time) {
  const progressBar = new SingleBar({
    format: "[{bar}]",
    barCompleteChar: "\u2588",
    barIncompleteChar: "\u2591",
    hideCursor: true,
  });

  // Configure the total number of iterations
  const totalIterations = 10;

  // Start the progress bar
  progressBar.start(totalIterations, 0);

  // Simulate a long-running process
  for (let i = 0; i < totalIterations; i++) {
    // Perform some operation here

    // Increment the progress bar
    progressBar.increment();

    // Simulate a delay between iterations
    // Replace this with your actual code
    // that takes time to process
    sleep(time);
  }

  // Stop the progress bar
  progressBar.stop();

  // Sleep function to simulate delay
  function sleep(milliseconds) {
    const start = Date.now();
    while (Date.now() - start < milliseconds) {
      // Do nothing, just wait
    }
  }
}

async function main() {
  // remove the previous data unless you use the -keep flag
  var argv = minimist(process.argv.slice(2));

  if (!argv.keep) {
    console.log("\n\nRemoving previously downloaded data and generated pages.");
    await showProgress(50);
    await removeAllFilesFromFolder("./src/data/journals");
    await removeAllFilesFromFolder("./src/content/journals");
    // await removeAllEntries();
  }

  // Request journal issn from user.
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  const issn = await new Promise((resolve) => {
    rl.question("\n\n\nEnter journal ISSN: ", (answer) => {
      rl.close();
      resolve(answer);
    });
  });

  await crossrefApiCall(issn);
}

// get a single url to get the data for the content pages
async function scrapMetaPage(doi) {
  // get the doi from the first work from the journal
  const doiUrl = `https://doi.org/${doi}`;
  let scrappedPages = await crossrefScrap(doiUrl);
  return JSON.stringify(scrappedPages);
}

// start the script
main();

function reorderPerIssue(data) {
  // console.log(data);

  // const jsonData = JSON.parse(data);
  // Parse the JSON data

  // Reorder the objects by volume and issue in reverse order
  const reorderedData = data.sort((a, b) => {
    if (a.volume !== b.volume) {
      return b.volume - a.volume;
    } else {
      return b.issue - a.issue;
    }
  });

  return reorderedData;
}

// regroup per volume
//
//
//

function reconstructVolumes(data) {
  const volumes = {};

  data.forEach((item) => {
    const { volume, issue, ...content } = item;

    if (!volumes[volume]) {
      volumes[volume] = [];
    }

    const issueObj = volumes[volume].find((issueObj) => {
      // Parse the issue numbers as integers for numeric comparison
      const issueNumber = parseInt(issueObj.number);
      const currentIssueNumber = parseInt(issue);

      return issueNumber === currentIssueNumber;
    });

    if (!issueObj) {
      volumes[volume].push({ number: issue, articles: [] });
    }

    volumes[volume]
      .find((issueObj) => {
        // Parse the issue numbers as integers for numeric comparison
        const issueNumber = parseInt(issueObj.number);
        const currentIssueNumber = parseInt(issue);

        return issueNumber === currentIssueNumber;
      })
      .articles.push({issue, volume, ...content});
  });

  const reconstructedVolumes = Object.entries(volumes).map(([volume, issues]) => {
    // Sort the issues based on the numeric value of the issue numbers
    const sortedIssues = issues.sort((a, b) => {
      const issueNumberA = parseInt(a.number);
      const issueNumberB = parseInt(b.number);

      return issueNumberA - issueNumberB;
    });

    return { volume, issues: sortedIssues };
  });

  return reconstructedVolumes;
}

