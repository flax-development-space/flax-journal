const fetch = require("node-fetch");
const fs = require("fs");

async function pullData() {
  const response = await fetch(
    "https://api.elifesciences.org/search?subject[]=neuroscience&page=1&per-page=1"
  );

  const data = await response.json();

  const ids = data?.items?.map((entry) => entry.id);

  const requests = ids?.map((id) => {
    if (isNaN(id)) return console.log(`${id} is not found`);
    return fetch(`https://api.elifesciences.org/articles/${id}`);
  });

  const responses = await Promise.all(requests);

  const entries = await Promise.all(
    responses.map(async (response) => {
      if (!response) return; // Add this check for undefined response
      const entry = await response.json();
      if (!entry) return;
      return {
        ...entry,
        ...data.items.find((item) => item.id === entry.id),
      };
    })
  );

  const collection = entries.map((entry) => ({
    id: entry?.id,
    ...data.item,
    ...entry,
  }));



  // filter to remove object without id

  const filteredCollection = collection.filter((obj) => {
    // Return true if the object is not empty and has a "foo" property
    return Object.keys(obj).length > 0 && obj.hasOwnProperty("title");
  });

  fs.writeFile(
    "src/data/neuroscience.json",
    JSON.stringify(filteredCollection),
    (err) => {
      if (err) {
        console.error(err);
        return;
      }
      console.log("Data written to file neuroscience.json!");
    }
  );

  console.log(collection);

  return collection;
}

pullData();


// remove all the content to have a simple example of all the available type 
