const fs = require("fs");
const axios = require("axios");
const path = require("path");
const readline = require("readline");

async function crossrefApiCall(issn) {
  // the content for the article in flax
  let articleContent = `
---
title: The articles pages
issn: "${issn.replace("-", "")}"
pagination:
  data: journals[${issn.replace("-", "")}]
  size: 1
  alias: article
permalink: "/articles/{{issn}}/{{article.volume}}/{{article.issue}}/{{article.DOI}}"
layout: "crossref/crossref-journal-single.njk"
---

`;

  let indexContent = `---
title: The articles
issn: "${issn.replace("-", "")}"
layout: crossref/crossref-journal-index.njk
permalink: "/articles/${issn.replace("-", "")}/"
menu: "issn: ${issn.replace("-", "")}"
---
`;

  //instead of going to get all the works with a doi parameters we can use the ISSN for the journal
  const url = `https://api.crossref.org/journals/${issn}/works?select=DOI,page,title,link,issue,volume,abstract,type,author&rows=200`;
  const headers = {
    "User-Agent": "Coko/1.0 (julien@coko.foundation)",
  };

  await axios
    .get(url, {
      headers: headers,
    })
    .then((res) => {
      console.log(
        `Journal ISSN:${issn} with a total of ${res.data.message["total-results"]} entries. We’re now pulling the data to generate the website`
      );
      if (res.data.message["total-results"] > 1000) {
        console.log(
          "We’re using only 1000 to keep crossreff api safe. Thanks for your understanding."
        );
      }
      const jsonData = JSON.stringify(res.data.message.items);
      fs.writeFileSync(
        `src/data/journals/${issn.replace("-", "")}.json`,
        jsonData,
        "utf8"
      );
      console.log("JSON file saved successfully.");

      // if the content/journals/ doesnt exist create it

      const folderPath = path.join(
        __dirname,
        "content",
        "journals",
        issn.replace("-", "")
      );

      // Check if the folder exists
      if (!fs.existsSync(folderPath)) {
        // Create the folder
        fs.mkdirSync(folderPath, { recursive: true });
        console.log(
          `Folder for issn: ${issn.replace("-", "")} created successfully.`
        );
      } else {
        console.log(
          `Folder for issn: ${issn.replace("-", "")} already exists.`
        );
      }

      fs.writeFileSync(
        `src/content/journals/${issn.replace("-", "")}-index.md`,
        indexContent,
        "utf8"
      );
      console.log("index page created file successfully.");

      fs.writeFileSync(
        `src/content/journals/${issn.replace("-", "")}-articles.md`,
        articleContent,
        "utf8"
      );
      console.log("article page created successfully.");
    })

    //create the eleventy files to make the content available
    //write index
    //write article page
    //fs

    .catch((err) => {
      console.log(err);
    });
}

async function main() {
  // Request journal DOI from user.
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  const issn = await new Promise((resolve) => {
    rl.question("Enter journal ISSN: ", (answer) => {
      rl.close();
      resolve(answer);
    });
  });

  crossrefApiCall(issn);
}

// start the script
main();
