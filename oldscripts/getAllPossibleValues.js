const fs = require("fs");

const jsonString = fs.readFileSync("./src/data/neuroscience.json");
const data = JSON.parse(jsonString);

const keys = getAllKeys(data);

// console.log(JSON.stringify(keys));

fs.writeFileSync("result.json", JSON.stringify(keys));

function getAllKeys(obj) {
  let keys = {};

  for (let key in obj) {
    if (typeof obj[key] === "object" && obj[key] !== null) {
      const nestedKeys = getAllKeys(obj[key]);
      keys[key] = { ...obj[key], ...nestedKeys };
    } else {
      keys[key] = true;
    }
  }

  return keys;
}
