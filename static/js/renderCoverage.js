// get metadata for all coverage
//
//

function getCoverage(journal) {
  // get data
  const journalDataAll = journal["coverage-type"].all;
  const journalDataCurrent = journal["coverage-type"].current;
  const journalDataBackfile = journal["coverage-type"].backfile;

  // "last-status-check-time",

  let journalMeta = {
    typologies: [
      {
        title: "All ",
        id: "all",
        data: journalDataAll,
      },
      {
        id: "current",
        title: "Current",
        data: journalDataCurrent,
      },
      {
        id: "back",
        title: "back",
        data: journalDataBackfile,
      },
    ],
  };

  // chart
  const fields = [
    "affiliations",
    "abstracts",
    "orcids",
    "licenses",
    "references",
    "funders",
    "similarity-checking",
    "award-numbers",
    "ror-ids",
    "update-policies",
    "resource-links",
    "descriptions",
  ];

  return generateCanvas(fields, journalMeta);
}

function renderCanvas() {
  let canvases = document.querySelectorAll(".coverage-canvas");
  canvases.forEach((canvas) => {
    generateDonut(canvas);
  });
}

function generateCanvas(fields, journalMeta) {
  let canvases = "";

  journalMeta.typologies.forEach((typology) => {
    console.log(typology);
    canvases += `<h3>${typology.title}</h3><ul>`;
    fields.forEach((field) => {
      const value = typology.data[field];
      canvases += `<li><canvas class="coverage-canvas" data-value="${value}" data-typology="${typology.id}" data-field="${field}" id="chart${typology.id}-${field}" width="300" height="300"></canvas>`;
    });

    canvases += `</ul>`;
  });
  return canvases;
}

function getPercentage(dataset) {
  const rounded = Object.entries(dataset).map((data) => {
    return { title: data[0], value: Number(data[1] * 100).toFixed(2) };
  });
  return rounded;
}

function generateDonut(element, dataset) {
  let fieldname = element.dataset.field;

  var ctx = element.getContext("2d");
  var donutChart = new Chart(ctx, {
    type: "doughnut",
    data: {
      labels: [fieldname],
      datasets: [
        {
          data: [element.dataset.value * 100, 100 - element.dataset.value * 100], // The values for each slice of the donut chart
          backgroundColor: ["rgb(75, 192, 192)", "lightgrey"], // The colors of each slice
          borderWidth: 1, // Width of the border around each slice
        },
      ],
    },
    options: {
      responsive: true,
      cutoutPercentage: 80, // Adjust the size of the center hole (0-100)
      legend: {
        position: "bottom", // Position of the legend (e.g., 'top', 'bottom', 'left', 'right')
      },
    },
  });
}

export { getCoverage, renderCanvas };
