function renderDoiChart(dataset, type, color) {

  var labels = dataset.map((item) => item[0]);
  var values = dataset.map((item) => item[1]);

  // Chart.js configuration
  var config = {
    type: type ? type : "line",
    data: {
      labels: labels,
      datasets: [
        {
          label: "DOI Evolution",
          data: values,
          fill: false,
          borderColor: color ? color : "rgb(75, 192, 192)",
          tension: 0.1,
        },
      ],
    },
    options: {
      responsive: true,
      scales: {
        x: {
          display: true,
          title: {
            display: true,
            text: "Year",
          },
        },
        y: {
          display: true,
          title: {
            display: true,
            text: "DOI count",
          },
        },
      },
    },
  };

  // Initialize the chart
  var ctx = document.getElementById("doiChart").getContext("2d");
  new Chart(ctx, config);
}

function reorderDois(array) {
  array.sort(function(a, b) {
    return a[0] - b[0];
  });
}

export { renderDoiChart, reorderDois };
