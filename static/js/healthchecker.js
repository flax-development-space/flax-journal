import { getCoverage, renderCanvas } from "./renderCoverage.js";
import { renderDoiChart, reorderDois } from "./renderDois.js";

async function fetchJournalDetails(local) {
  // Get the ISSN value from the input field
  var issn = document.getElementById("issn").value;

  function crossrefDoiStatus(journal) {
    const dois = `<p class="current">Curent DOI: ${journal.counts["current-dois"]}</p>
<p class="backfile">Backfile DOI: ${journal.counts["backfile-dois"]}</p>
<p class="total">Total DOI: ${journal.counts["total-dois"]}</p> 
<canvas id="doiChart" width="400" height="200"></canvas>`;

    return dois;
  }

  function crossrefCoverage(journal) {
    return getCoverage(journal);
  }

  function crossrefSubjects(journal) {
    let subjects = journal.subjects.map(
      (subj) =>
        `<li><span class="subject-name">${subj.name}</span> <span class="subject-id">[${subj.ASJC}]</span></li>`
    );
    return subjects.join("");
  }
  // Make a GET request to the Crossref API

  const url = local
    ? "/assets/journals/ISSNdemometa.json"
    : `https://api.crossref.org/journals/${issn}`;

  axios
    .get(url)
    .then(function(response) {
      // Extract relevant data from the response
      // console.log(response.data.subjects);
      var data = response.data;
      const journal = local ? response.data : data.message;

      // console.log(journal);

      // Populate the #results element with the retrieved data
      var resultsElement = document.getElementById("results");
      resultsElement.innerHTML = `
          <h2>${journal.title}</h2>
          <p class="publisher">${journal.publisher}</p>

          <section class="subjects-wrap">
          <h3>Subjects</h3>
          <ul class="subjects">
            ${crossrefSubjects(journal)}
          </ul>
          </section>

          <section class="doi-wrap">
          <h3>DOI count</h3>
            ${crossrefDoiStatus(journal)}
          </section>

          <section class="coverage-wrap">
            ${crossrefCoverage(journal)}
          </section>`;

      return journal;
    })
    .then(function(journal) {
      // console.log(journal.breakdowns["dois-by-issued-year"]);
      let dois = journal.breakdowns["dois-by-issued-year"].sort(function(
        a,
        b
      ) {
        return a[0] - b[0];
      });

      renderDoiChart(dois);
    })
    .then(() => {
      console.log("rendercanvas now");
      renderCanvas();
    })
    .catch(function(error) {
      // Handle any errors that occur during the request
      console.log(error);
    });
}

export { fetchJournalDetails };
