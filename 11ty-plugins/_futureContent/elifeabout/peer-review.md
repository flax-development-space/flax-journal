---
title: Publishing and peer review at eLife
baseline: "Reviewed Preprints combine the advantages of preprints with the scrutiny offered by peer review."
metatitle: Publishing and peer review
metalinktext: How publishing with eLife works
metaintro: eLife publishes Reviewed Preprints that combine the advantages of preprints with the scrutiny offered by peer review.
metaimg: peer-review.png
relatedLinks: 
  -  "Read our [Aims and scope](./aims-scope)"
  -  "Learn more about the [eLife editors](https://elifesciences.org/about/people)"
  -  "[Submit to eLife](https://elifesciences.org/submit-your-research)"
  -  "Read our [Author Guide](https://elife-rp.msubmit.net/html/elife-rp_author_instructions.html#process) and [Reviewer Guide](https://elife-rp.msubmit.net/cgi-bin/main.plex?form_type=display_rev_instructions#process)"
  -  "Read about [eLife’s peer-review process](http://elifesciences.org/peer-review-process)"
  -  "Read about [eLife assessments](http://elifesciences.org/inside-elife/db24dd46)"
  -  "Read about [funders supporting the use of Reviewed Preprints in research assessment](https://elifesciences.org/inside-elife/ebadb0f1)"
  -  "[FAQs](https://elife-rp.msubmit.net/html/elife-rp_author_instructions.html#faq)"
  -  "[Submit to eLife’s traditional model](https://submit.elifesciences.org/)"
---

eLife reviews preprints in the life sciences and medicine, and is committed to improving peer review to better convey the assessments made by editors and reviewers. Our approach brings together the immediacy and openness of a preprint with the scrutiny of peer review by experts.

![Diagram showing the steps to publishing at eLife. These are Submit your research, decision to review, consultative peer review, Reviewed Preprint published, provide your revisions (optional) and version of record (optional).](/assets/images/about/peer-review2.png)

The main features of this approach are:

- We only review articles that have been made available as preprints.
- Editors who are active researchers decide which preprints are reviewed.
- Editors and reviewers discuss the reviews with each other.
- There is no accept/reject decision after peer review: rather, every article we review is published on the eLife website as a Reviewed Preprint that includes an eLife assessment, public reviews, and a response from the authors (if available).
- We do not artificially limit the number of articles we review or publish.
- At any point following peer review, authors can choose to have their Reviewed Preprint published as a regular journal article. This version (known as the Version of Record) will be sent to indexers including PubMed, Scopus and Web of Science.
- The fee for publishing an article with eLife is $2,000, charged at the point we commit to peer reviewing the article.
- Publication of an eLife Version of Record complies with all major funding agency requirements for immediate online access to the published results of their research grants. An increasing number of funders are also recognising Reviewed Preprints in research assessment. 

The eLife peer-review process produces the following outputs:

1. An eLife assessment that summarises the significance of the findings and the strength of the evidence; the editor and the reviewers write this assessment with the help of a common vocabulary to ensure consistency.
2. Public reviews that describe the strengths and weaknesses of the article, and indicate whether the claims and conclusions are justified by the data.
3. Recommendations for the authors, including suggestions for revising and improving the article. These recommendations are not included in the Reviewed Preprint.

The model of peer review described above was introduced in January 2023. For a limited period authors can ask for submissions to be reviewed under eLife’s traditional model (which includes an accept/reject decision after peer review).
